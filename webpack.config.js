const path = require('path'),
  { BannerPlugin } = require('webpack')

module.exports = {
  // devtool: 'source-map',
  entry: {
    index: './src/index.ts',
    cli: './src/cli.ts',
  },
  mode: 'production',
  externals: [ 'aws-sdk' ],
  resolve: {
    symlinks: false,
    extensions: [ '.js', '.json', '.ts' ],
  },
  output: {
    libraryTarget: 'commonjs2',
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
  },
  target: 'node',
  plugins: [
    new BannerPlugin({
      banner: '#!/usr/bin/env node',
      raw: true,
      test: 'cli.js',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.[jt]sx?$/,
        loader: 'esbuild-loader',
        options: {
          target: 'es2020',
          loader: 'ts',
        },
      },
    ],
  },
}

