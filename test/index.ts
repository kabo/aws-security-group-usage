/* global describe, expect, it, jest */
/* eslint-disable better/explicit-return, fp/no-unused-expression, fp/no-nil */
import { eniIsDms, eniIsElb, eniIsElastiCache, eniIsInstance, eniIsLambda, eniIsRds, eniIsR53Resolver, eniIsVpce, eniIsXlb } from '../src/index'

describe('index', () => {
  describe('eniIsInstance', () => {
    it('recognizes instance ENI', () => {
      const input = {
        Attachment: {
          InstanceId: 'i-123',
        },
      }
      expect(eniIsInstance(input)).toBe(true)
    })
    it('handles empty Attachment', () => {
      const input = { Attachment: {} }
      expect(eniIsInstance(input)).toBe(false)
    })
    it('handles no Attachment', () => {
      const input = {}
      expect(eniIsInstance(input)).toBe(false)
    })
  })

  describe('eniIsElb', () => {
    it('recognizes ELB ENI', () => {
      const input = {
        RequesterId: 'amazon-elb',
        Description: 'ELB abc',
      }
      expect(eniIsElb(input)).toBe(true)
    })
    it('handles ALB', () => {
      const input = {
        RequesterId: 'amazon-elb',
        Description: 'ELB app/abc',
      }
      expect(eniIsElb(input)).toBe(false)
    })
    it('handles NLB', () => {
      const input = {
        RequesterId: 'amazon-elb',
        Description: 'ELB net/abc',
      }
      expect(eniIsElb(input)).toBe(false)
    })
  })

  describe('eniIsXlb', () => {
    it('recognizes ALB ENI', () => {
      const input = {
        RequesterId: 'amazon-elb',
        Description: 'ELB app/abc',
      }
      expect(eniIsXlb(input)).toBe(true)
    })
    it('recognizes NLB ENI', () => {
      const input = {
        RequesterId: 'amazon-elb',
        Description: 'ELB net/abc',
      }
      expect(eniIsXlb(input)).toBe(true)
    })
    it('handles ELB', () => {
      const input = {
        RequesterId: 'amazon-elb',
        Description: 'ELB abc',
      }
      expect(eniIsXlb(input)).toBe(false)
    })
  })

  describe('eniIsVpce', () => {
    it('recognizes Vpce ENI', () => {
      const input = { InterfaceType: 'vpc_endpoint' }
      expect(eniIsVpce(input)).toBe(true)
    })
    it('handles non-Vpce ENI', () => {
      const input = { InterfaceType: 'interface' }
      expect(eniIsVpce(input)).toBe(false)
    })
  })

  describe('eniIsRds', () => {
    it('recognizes RDS ENI', () => {
      const input = { RequesterId: 'amazon-rds' }
      expect(eniIsRds(input)).toBe(true)
    })
    it('handles non-RDS ENI', () => {
      const input = { RequesterId: 'amazon-aws' }
      expect(eniIsRds(input)).toBe(false)
    })
  })

  describe('eniIsLambda', () => {
    it('recognizes Lambda ENI', () => {
      const input = { InterfaceType: 'lambda' }
      expect(eniIsLambda(input)).toBe(true)
    })
    it('handles non-Lambda ENI', () => {
      const input = { InterfaceType: 'interface' }
      expect(eniIsLambda(input)).toBe(false)
    })
  })

  describe('eniIsElastiCache', () => {
    it('recognizes ElastiCache ENI', () => {
      const input = { RequesterId: 'amazon-elasticache' }
      expect(eniIsElastiCache(input)).toBe(true)
    })
    it('handles non-ElastiCache ENI', () => {
      const input = { RequesterId: 'amazon-aws' }
      expect(eniIsElastiCache(input)).toBe(false)
    })
  })

  describe('eniIsR53Resolver', () => {
    it('recognizes Route53Resolver ENI', () => {
      const input = { Description: 'Route 53 Resolver: abc' }
      expect(eniIsR53Resolver(input)).toBe(true)
    })
    it('handles non-Route53Resolver ENI', () => {
      const input = { Description: 'ELB abc' }
      expect(eniIsR53Resolver(input)).toBe(false)
    })
  })

  describe('eniIsDms', () => {
    it('recognizes DMS ENI', () => {
      const input = { Description: 'DMSNetworkInterface' }
      expect(eniIsDms(input)).toBe(true)
    })
    it('handles non-DMS ENI', () => {
      const input = { Description: 'ELB abc' }
      expect(eniIsDms(input)).toBe(false)
    })
  })
})
