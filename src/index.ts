import { DMS, EC2, ElastiCache, ELB, ELBv2, Lambda, RDS, Route53Resolver } from 'aws-sdk'
import PQueue from 'p-queue'
import RallPass from 'ramda/src/allPass'
import Ralways from 'ramda/src/always'
import Rany from 'ramda/src/any'
import RanyPass from 'ramda/src/anyPass'
import Rcomplement from 'ramda/src/complement'
import Rconcat from 'ramda/src/concat'
import Rdrop from 'ramda/src/drop'
import Rfilter from 'ramda/src/filter'
import RhasPath from 'ramda/src/hasPath'
import RifElse from 'ramda/src/ifElse'
import Rincludes from 'ramda/src/includes'
import RisEmpty from 'ramda/src/isEmpty'
import Rlast from 'ramda/src/last'
import RlensProp from 'ramda/src/lensProp'
import Rmap from 'ramda/src/map'
import RmemoizeWith from 'ramda/src/memoizeWith'
import Rnot from 'ramda/src/not'
import Rpath from 'ramda/src/path'
import RpathSatisfies from 'ramda/src/pathSatisfies'
import Rpipe from 'ramda/src/pipe'
import Rprop from 'ramda/src/prop'
import RpropEq from 'ramda/src/propEq'
import RpropSatisfies from 'ramda/src/propSatisfies'
import Rset from 'ramda/src/set'
import Rsplit from 'ramda/src/split'
import RstartsWith from 'ramda/src/startsWith'
// import Rtap from 'ramda/src/tap'

interface ListSecurityGroupUsageOptions {
  readonly groupIds?: string[]
  readonly ec2?: EC2
  readonly elb?: ELB
  readonly elbv2?: ELBv2
  readonly rds?: RDS
  readonly lambda?: Lambda
  readonly ec?: ElastiCache
  readonly r53r?: Route53Resolver
  readonly dms?: DMS
  readonly queue?: PQueue
}
interface SecurityGroupUsageOptions {
  readonly groupIds: string[]
  readonly ec2: EC2
  readonly elb: ELB
  readonly elbv2: ELBv2
  readonly rds: RDS
  readonly lambda: Lambda
  readonly ec: ElastiCache
  readonly r53r: Route53Resolver
  readonly dms: DMS
  readonly queue: PQueue
}
interface ListSecurityGroupsOptions {
  readonly groupIds: string[]
  readonly ec2: EC2
}
interface SecurityGroupWithEnis extends EC2.Types.SecurityGroup {
  readonly enis: EC2.Types.NetworkInterfaceList
}
interface InstanceUsage extends EC2.Types.Instance {
  readonly usagetype: 'instance'
}
interface ElbUsage extends ELB.Types.LoadBalancerDescription {
  readonly usagetype: 'elb'
}
interface XlbUsage extends ELBv2.Types.LoadBalancer {
  readonly usagetype: 'xlb'
}
interface VpceUsage extends EC2.Types.VpcEndpoint {
  readonly usagetype: 'vpce'
}
interface RdsUsage extends RDS.Types.DBInstance {
  readonly usagetype: 'rds'
}
interface LambdaUsage extends Lambda.Types.FunctionConfiguration {
  readonly usagetype: 'lambda'
}
interface ElastiCacheUsage extends ElastiCache.Types.CacheCluster {
  readonly usagetype: 'elasticache'
}
interface Route53ResolverUsage extends Route53Resolver.Types.ResolverEndpoint {
  readonly usagetype: 'route53resolver'
}
interface DmsUsage extends DMS.Types.ReplicationInstance {
  readonly usagetype: 'dms'
}
// type InstanceUsages = readonly InstanceUsage[]
export type Usage = InstanceUsage | ElbUsage | XlbUsage | VpceUsage | RdsUsage | LambdaUsage | ElastiCacheUsage | Route53ResolverUsage | DmsUsage
type Usages = readonly Usage[]
export interface SecurityGroupWithUsage extends SecurityGroupWithEnis {
  readonly usage: Usages
}
export type SecurityGroupsWithUsage = readonly SecurityGroupWithUsage[]

const
  truthy = Rcomplement(Rnot),
  promiseAll = <T>(x: Promise<T>[]): Promise<T[]> => Promise.all(x),
  addDefaults = ({ dms, ec2, ec, elb, elbv2, lambda, rds, r53r, groupIds, queue }: ListSecurityGroupUsageOptions): Promise<SecurityGroupUsageOptions> =>
    Promise.resolve({
      ec2: ec2 ?? new EC2(),
      elb: elb ?? new ELB(),
      elbv2: elbv2 ?? new ELBv2(),
      rds: rds ?? new RDS(),
      lambda: lambda ?? new Lambda(),
      ec: ec ?? new ElastiCache(),
      r53r: r53r ?? new Route53Resolver(),
      dms: dms ?? new DMS(),
      groupIds: groupIds ?? [],
      queue: queue ?? new PQueue({ concurrency: 20 }),
    }),
  listSecurityGroups = ({ ec2, groupIds }: ListSecurityGroupsOptions, NextToken?: string): Promise<EC2.Types.SecurityGroupList> =>
    ec2.describeSecurityGroups({ GroupIds: groupIds, NextToken }).promise()
      .then(({ SecurityGroups = [], NextToken }) =>
        !NextToken
          ? SecurityGroups
          : listSecurityGroups({ ec2, groupIds }, NextToken).then(Rconcat(SecurityGroups))),
  getEnisForSg = (ec2: EC2, sg: EC2.Types.SecurityGroup, NextToken?: string): Promise<EC2.Types.NetworkInterfaceList> =>
    ec2.describeNetworkInterfaces({ NextToken, Filters: [ { Name: 'group-id', Values: [ sg.GroupId as string ] } ] }).promise()
      .then(({ NetworkInterfaces = [], NextToken }) =>
        !NextToken
          ? NetworkInterfaces
          : getEnisForSg(ec2, sg, NextToken).then(Rconcat(NetworkInterfaces))),
  addEnis = (ec2: EC2, queue: PQueue) => (sg: EC2.Types.SecurityGroup): Promise<SecurityGroupWithEnis> =>
    queue.add(() => getEnisForSg(ec2, sg)
      .then((enis) => ({ ...sg, enis }))),
  getInstance = RmemoizeWith(Rprop('InstanceId'), ({ ec2, InstanceId }: { ec2: EC2, InstanceId: string }) =>
    ec2.describeInstances({ InstanceIds: [ InstanceId ] }).promise()
      .then(Rpath([ 'Reservations', 0, 'Instances', 0 ]) as (x: EC2.Types.DescribeInstancesResult) => EC2.Types.Instance)),
  getInstanceForEni = (ec2: EC2, queue: PQueue) => ({ Attachment }: EC2.Types.NetworkInterface): Promise<EC2.Types.Instance> =>
    queue.add(() => getInstance({ ec2, InstanceId: Attachment?.InstanceId as string })),
  addInstances = (ec2: EC2, queue: PQueue) => (sg: SecurityGroupWithEnis): Promise<SecurityGroupWithUsage> =>
    Promise.resolve(sg.enis)
      .then(Rfilter(eniIsInstance))
      .then(Rmap(getInstanceForEni(ec2, queue)))
      .then(promiseAll as (x: Promise<EC2.Types.Instance>[]) => Promise<EC2.Types.InstanceList>)
      .then(Rmap(Rset(RlensProp('usagetype'), 'instance') as (x: EC2.Types.Instance) => InstanceUsage))
      .then((usage: Usages) => ({ ...sg, usage })),
  addUsage = (sg: SecurityGroupWithUsage) => (usage: Usages): SecurityGroupWithUsage => ({
    ...sg,
    usage: (Rconcat as any)(sg.usage, usage),
  }),
  getElb = RmemoizeWith(Rprop('LoadBalancerName'), ({ elb, LoadBalancerName }: { elb: ELB, LoadBalancerName: string }) =>
    elb.describeLoadBalancers({ LoadBalancerNames: [ LoadBalancerName ] }).promise()
      .then(Rpath([ 'LoadBalancerDescriptions', 0 ]) as (x: ELB.Types.DescribeAccessPointsOutput) => ELB.Types.LoadBalancerDescription)),
  getElbName = Rdrop(4),
  getElbForEni = (elb: ELB, queue: PQueue) => ({ Description }: EC2.Types.NetworkInterface): Promise<ELB.Types.LoadBalancerDescription> =>
    queue.add(() => getElb({ elb, LoadBalancerName: getElbName(Description as string) })),
  addElbs = (elb: ELB, queue: PQueue) => (sg: SecurityGroupWithUsage): Promise<SecurityGroupWithUsage> =>
    Promise.resolve(sg.enis)
      .then(Rfilter(eniIsElb))
      .then(Rmap(getElbForEni(elb, queue)))
      .then(promiseAll as (x: Promise<ELB.Types.LoadBalancerDescription>[]) => Promise<ELB.Types.LoadBalancerDescriptions>)
      .then(Rmap(Rset(RlensProp('usagetype'), 'elb') as (x: ELB.Types.LoadBalancerDescription) => ElbUsage))
      .then(addUsage(sg)),
  getXlb = RmemoizeWith(Rprop('LoadBalancerName'), ({ elbv2, LoadBalancerName }: { elbv2: ELBv2, LoadBalancerName: string }) =>
    elbv2.describeLoadBalancers({ Names: [ LoadBalancerName ] }).promise()
      .then(Rpath([ 'LoadBalancers', 0 ]) as (x: ELBv2.Types.DescribeLoadBalancersOutput) => ELBv2.Types.LoadBalancer)),
  getXlbName = Rpipe<string, string[], string>(
    Rsplit('/'),
    (Rprop as any)(1) as (x: string[]) => string
  ),
  getXlbForEni = (elbv2: ELBv2, queue: PQueue) => ({ Description }: EC2.Types.NetworkInterface): Promise<ELBv2.Types.LoadBalancer> =>
    queue.add(() => getXlb({ elbv2, LoadBalancerName: getXlbName(Description as string) })),
  addXlbs = (elbv2: ELBv2, queue: PQueue) => (sg: SecurityGroupWithUsage): Promise<SecurityGroupWithUsage> =>
    Promise.resolve(sg.enis)
      .then(Rfilter(eniIsXlb))
      .then(Rmap(getXlbForEni(elbv2, queue)))
      .then(promiseAll as (x: Promise<ELBv2.Types.LoadBalancer>[]) => Promise<ELBv2.Types.LoadBalancers>)
      .then(Rmap(Rset(RlensProp('usagetype'), 'xlb') as (x: ELBv2.Types.LoadBalancer) => XlbUsage))
      .then(addUsage(sg)),
  getVpce = RmemoizeWith(Rprop('VpcEndpointId'), ({ ec2, VpcEndpointId }: { ec2: EC2, VpcEndpointId: string }) =>
    ec2.describeVpcEndpoints({ VpcEndpointIds: [ VpcEndpointId ] }).promise()
      .then(Rpath([ 'VpcEndpoints', 0 ]) as (x: EC2.Types.DescribeVpcEndpointsResult) => EC2.Types.VpcEndpoint)),
  getVpceId = Rpipe<string, string[], string>(
    Rsplit(' '),
    Rlast
  ),
  getVpceForEni = (ec2: EC2, queue: PQueue) => ({ Description }: EC2.Types.NetworkInterface): Promise<EC2.Types.VpcEndpoint> =>
    queue.add(() => getVpce({ ec2, VpcEndpointId: getVpceId(Description as string) })),
  addVpcEndpoints = (ec2: EC2, queue: PQueue) => (sg: SecurityGroupWithUsage): Promise<SecurityGroupWithUsage> =>
    Promise.resolve(sg.enis)
      .then(Rfilter(eniIsVpce))
      .then(Rmap(getVpceForEni(ec2, queue)))
      .then(promiseAll as (x: Promise<EC2.Types.VpcEndpoint>[]) => Promise<EC2.Types.VpcEndpointSet>)
      .then(Rmap(Rset(RlensProp('usagetype'), 'vpce') as (x: EC2.Types.VpcEndpoint) => VpceUsage))
      .then(addUsage(sg)),
  getAccesskeyFromClient = (client: string) => Rpath([ client, 'config', 'credentials', 'accessKeyId' ]) as (obj: any) => string,
  getRds = ({ rds, Marker }: { rds: RDS, Marker?: string }): Promise<RDS.Types.DBInstanceList> =>
    rds.describeDBInstances({ Marker }).promise()
      .then(({ DBInstances = [], Marker }) =>
        !Marker
          ? DBInstances
          : getRds({ rds, Marker }).then(Rconcat(DBInstances))),
  getRdsMemoized = RmemoizeWith(getAccesskeyFromClient('rds'), getRds),
  getRdsForSg = (rds: RDS, { GroupId }: EC2.Types.SecurityGroup): Promise<RDS.Types.DBInstanceList> =>
    getRdsMemoized({ rds })
      .then(Rfilter(RpropSatisfies(Rany(RpropEq('VpcSecurityGroupId', GroupId)), 'VpcSecurityGroups'))),
  addRds = (rds: RDS, queue: PQueue) => (sg: SecurityGroupWithUsage): Promise<SecurityGroupWithUsage> =>
    Promise.resolve(sg.enis)
      .then(Rfilter(eniIsRds))
      .then(RifElse(
        RisEmpty,
        Ralways([]),
        () => queue.add(() => getRdsForSg(rds, sg))
      ))
      .then(Rmap(Rset(RlensProp('usagetype'), 'rds') as (x: RDS.Types.DBInstance) => RdsUsage))
      .then(addUsage(sg)),
  getLambdas = ({ lambda, Marker }: { lambda: Lambda, Marker?: string }): Promise<Lambda.Types.FunctionList> =>
    lambda.listFunctions({ Marker }).promise()
      .then(({ Functions = [], NextMarker }) =>
        !NextMarker
          ? Functions
          : getLambdas({ lambda, Marker: NextMarker }).then(Rconcat(Functions))),
  getLambdasMemoized = RmemoizeWith(getAccesskeyFromClient('lambda'), getLambdas),
  getLambdasForSg = (lambda: Lambda, { GroupId }: EC2.Types.SecurityGroup): Promise<Lambda.Types.FunctionList> =>
    getLambdasMemoized({ lambda })
      .then(Rfilter(RallPass([
        RhasPath([ 'VpcConfig', 'SecurityGroupIds' ]),
        RpathSatisfies(Rincludes(GroupId), [ 'VpcConfig', 'SecurityGroupIds' ]),
      ]))),
  addLambdas = (lambda: Lambda, queue: PQueue) => (sg: SecurityGroupWithUsage): Promise<SecurityGroupWithUsage> =>
    Promise.resolve(sg.enis)
      .then(Rfilter(eniIsLambda))
      .then(RifElse(
        RisEmpty,
        Ralways([]),
        () => queue.add(() => getLambdasForSg(lambda, sg))
      ))
      .then(Rmap(Rset(RlensProp('usagetype'), 'lambda') as (x: Lambda.Types.FunctionConfiguration) => LambdaUsage))
      .then(addUsage(sg)),
  getElastiCache = ({ ec, Marker }: { ec: ElastiCache, Marker?: string }): Promise<ElastiCache.Types.CacheClusterList> =>
    ec.describeCacheClusters({ Marker }).promise()
      .then(({ CacheClusters = [], Marker }) =>
        !Marker
          ? CacheClusters
          : getElastiCache({ ec, Marker }).then(Rconcat(CacheClusters))),
  getElastiCacheMemoized = RmemoizeWith(getAccesskeyFromClient('ec'), getElastiCache),
  getElastiCacheForSg = (ec: ElastiCache, { GroupId }: EC2.Types.SecurityGroup): Promise<ElastiCache.Types.CacheClusterList> =>
    getElastiCacheMemoized({ ec })
      .then(Rfilter(RpropSatisfies(Rany(RpropEq('SecurityGroupId', GroupId)), 'SecurityGroups'))),
  addElastiCache = (ec: ElastiCache, queue: PQueue) => (sg: SecurityGroupWithUsage): Promise<SecurityGroupWithUsage> =>
    Promise.resolve(sg.enis)
      .then(Rfilter(eniIsElastiCache))
      .then(RifElse(
        RisEmpty,
        Ralways([]),
        () => queue.add(() => getElastiCacheForSg(ec, sg))
      ))
      .then(Rmap(Rset(RlensProp('usagetype'), 'elasticache') as (x: ElastiCache.Types.CacheCluster) => ElastiCacheUsage))
      .then(addUsage(sg)),
  getR53ResolversForSg = (r53r: Route53Resolver, sg: EC2.Types.SecurityGroup, NextToken?: string): Promise<Route53Resolver.Types.ResolverEndpoints> =>
    r53r.listResolverEndpoints({ NextToken, Filters: [ { Name: 'SecurityGroupIds', Values: [ sg.GroupId as string ] } ] }).promise()
      .then(({ ResolverEndpoints = [], NextToken }) =>
        !NextToken
          ? ResolverEndpoints
          : getR53ResolversForSg(r53r, sg, NextToken).then(Rconcat(ResolverEndpoints))),
  addR53Resolvers = (r53r: Route53Resolver, queue: PQueue) => (sg: SecurityGroupWithUsage): Promise<SecurityGroupWithUsage> =>
    Promise.resolve(sg.enis)
      .then(Rfilter(eniIsR53Resolver))
      .then(RifElse(
        RisEmpty,
        Ralways([]),
        () => queue.add(() => getR53ResolversForSg(r53r, sg))
      ))
      .then(Rmap(Rset(RlensProp('usagetype'), 'route53resolver') as (x: Route53Resolver.Types.ResolverEndpoint) => Route53ResolverUsage))
      .then(addUsage(sg)),
  getDms = ({ dms, Marker }: { dms: DMS, Marker?: string }): Promise<DMS.Types.ReplicationInstanceList> =>
    dms.describeReplicationInstances({ Marker }).promise()
      .then(({ ReplicationInstances = [], Marker }) =>
        !Marker
          ? ReplicationInstances
          : getDms({ dms, Marker }).then(Rconcat(ReplicationInstances))),
  getDmsMemoized = RmemoizeWith(getAccesskeyFromClient('dms'), getDms),
  getDmsForSg = (dms: DMS, { GroupId }: EC2.Types.SecurityGroup): Promise<DMS.Types.ReplicationInstanceList> =>
    getDmsMemoized({ dms })
      .then(Rfilter(RpropSatisfies(Rany(RpropEq('VpcSecurityGroupId', GroupId)), 'VpcSecurityGroups'))),
  addDms = (dms: DMS, queue: PQueue) => (sg: SecurityGroupWithUsage): Promise<SecurityGroupWithUsage> =>
    Promise.resolve(sg.enis)
      .then(Rfilter(eniIsDms))
      .then(RifElse(
        RisEmpty,
        Ralways([]),
        () => queue.add(() => getDmsForSg(dms, sg))
      ))
      .then(Rmap(Rset(RlensProp('usagetype'), 'dms') as (x: DMS.Types.ReplicationInstance) => DmsUsage))
      .then(addUsage(sg))

export const eniIsInstance = RpathSatisfies(truthy, [ 'Attachment', 'InstanceId' ])
export const eniIsElb = RallPass([
  RpropEq('RequesterId', 'amazon-elb'),
  RallPass([
    RpropSatisfies(Rcomplement(RstartsWith('ELB app/')), 'Description'),
    RpropSatisfies(Rcomplement(RstartsWith('ELB net/')), 'Description'),
  ]),
])
export const eniIsXlb = RallPass([
  RpropEq('RequesterId', 'amazon-elb'),
  RanyPass([
    RpropSatisfies(RstartsWith('ELB app/'), 'Description'),
    RpropSatisfies(RstartsWith('ELB net/'), 'Description'),
  ]),
])
export const eniIsVpce = RpropEq('InterfaceType', 'vpc_endpoint') as (x: EC2.Types.NetworkInterface) => boolean
export const eniIsRds = RpropEq('RequesterId', 'amazon-rds') as (x: EC2.Types.NetworkInterface) => boolean
export const eniIsLambda = RpropEq('InterfaceType', 'lambda') as (x: EC2.Types.NetworkInterface) => boolean
export const eniIsElastiCache = RpropEq('RequesterId', 'amazon-elasticache') as (x: EC2.Types.NetworkInterface) => boolean
export const eniIsR53Resolver = RpropSatisfies(RstartsWith('Route 53 Resolver: '), 'Description')
export const eniIsDms = RpropEq('Description', 'DMSNetworkInterface') as (x: EC2.Types.NetworkInterface) => boolean

export const listSecurityGroupUsage = (opts: ListSecurityGroupUsageOptions): Promise<SecurityGroupsWithUsage> =>
  addDefaults(opts)
    .then(({ dms, ec2, ec, elb, elbv2, lambda, r53r, rds, groupIds, queue }: SecurityGroupUsageOptions) =>
      listSecurityGroups({ ec2, groupIds })
        .then(Rmap(addEnis(ec2, queue)))
        .then(promiseAll)
        .then(Rmap(addInstances(ec2, queue)))
        .then(promiseAll)
        .then(Rmap(addElbs(elb, queue)))
        .then(promiseAll)
        .then(Rmap(addXlbs(elbv2, queue)))
        .then(promiseAll)
        .then(Rmap(addVpcEndpoints(ec2, queue)))
        .then(promiseAll)
        .then(Rmap(addRds(rds, queue)))
        .then(promiseAll)
        .then(Rmap(addLambdas(lambda, queue)))
        .then(promiseAll)
        .then(Rmap(addElastiCache(ec, queue)))
        .then(promiseAll)
        .then(Rmap(addR53Resolvers(r53r, queue)))
        .then(promiseAll)
        .then(Rmap(addDms(dms, queue)))
        .then(promiseAll)
    )
