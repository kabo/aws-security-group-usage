// @ts-ignore
import tree from 'pretty-tree'
import PQueue from 'p-queue'
import Ralways from 'ramda/src/always'
import Rcond from 'ramda/src/cond'
import Rmap from 'ramda/src/map'
import Rpick from 'ramda/src/pick'
import RpropEq from 'ramda/src/propEq'
import RT from 'ramda/src/T'
// import Rtap from 'ramda/src/tap'
import yargs from 'yargs'
// @ts-ignore
import { hideBin } from 'yargs/helpers'
import { listSecurityGroupUsage, SecurityGroupsWithUsage, SecurityGroupWithUsage, Usage } from './index'

interface Argv {
  readonly groupIds: string[]
  readonly output: string
  readonly concurrency: number
}

const
  getArgs = (argv: any): Promise<Argv> =>
    Promise.resolve(yargs(hideBin(argv))
      .option('c', {
        alias: 'concurrency',
        type: 'number',
        describe: 'max number of concurrent requests to make',
        default: 20,
      })
      .option('g', {
        alias: 'groupIds',
        type: 'array',
        describe: 'security group ids to check',
        default: [],
      })
      .option('o', {
        alias: 'output',
        type: 'string',
        describe: 'output format',
        choices: [ 'tree', 'json' ],
        default: 'tree',
      })
      .help()
      .argv as any),
  stringify = (sgs: SecurityGroupsWithUsage): string => JSON.stringify(sgs, null, 2), // eslint-disable-line fp/no-nil
  isInstanceUsage = RpropEq('usagetype', 'instance'),
  isElbUsage = RpropEq('usagetype', 'elb'),
  isXlbUsage = RpropEq('usagetype', 'xlb'),
  isVpceUsage = RpropEq('usagetype', 'vpce'),
  isRdsUsage = RpropEq('usagetype', 'rds'),
  isLambdaUsage = RpropEq('usagetype', 'lambda'),
  isElastiCacheUsage = RpropEq('usagetype', 'elasticache'),
  isR53ResolverUsage = RpropEq('usagetype', 'route53resolver'),
  isDmsUsage = RpropEq('usagetype', 'dms'),
  usageToLeaf = Rcond([
    [ isInstanceUsage, Rpick([ 'InstanceId' ]) ],
    [ isElbUsage, Rpick([ 'LoadBalancerName' ]) ],
    [ isXlbUsage, Rpick([ 'LoadBalancerName', 'Type' ]) ],
    [ isVpceUsage, Rpick([ 'VpcEndpointId' ]) ],
    [ isRdsUsage, Rpick([ 'DBInstanceIdentifier' ]) ],
    [ isLambdaUsage, Rpick([ 'FunctionName' ]) ],
    [ isElastiCacheUsage, Rpick([ 'CacheClusterId' ]) ],
    [ isR53ResolverUsage, Rpick([ 'Id' ]) ],
    [ isDmsUsage, Rpick([ 'ReplicationInstanceIdentifier' ]) ],
    [ RT, Ralways({}) ],
  ]),
  usageToNode = (u: Usage) => ({
    label: `usage type: ${u.usagetype}`,
    leaf: usageToLeaf(u),
  }),
  sgToNode = (sg: SecurityGroupWithUsage) => ({
    label: `${sg.GroupId}: ${sg.GroupName}`,
    nodes: Rmap(usageToNode, sg.usage),
  }),
  toTree = (sgs: SecurityGroupsWithUsage): string => tree({
    label: '',
    nodes: Rmap(sgToNode, sgs),
  }),
  format = (f: string) =>
    f === 'json'
      ? stringify
      : toTree

// eslint-disable-next-line fp/no-unused-expression
getArgs(process.argv)
  .then(({ groupIds, concurrency, output }: Argv) =>
    listSecurityGroupUsage({ groupIds, queue: new PQueue({ concurrency }) })
      // .then(Rtap(console.log))
      .then(format(output))
  )
  .then(console.log)
  .catch(console.error)
