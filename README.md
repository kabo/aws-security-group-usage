# AWS Security Group Usage

> List AWS security group dependencies

See where your security groups are used.

## Install

Install globally if you want to use the CLI.

```sh
yarn global add aws-security-group-usage
# or
npm i -g aws-security-group-usage
```

Install locally if you want to use it as a library.

```sh
yarn add aws-security-group-usage
# or
npm i --save aws-security-group-usage
```

## Usage

### CLI

```sh
$ aws-security-group-usage --help
Options:
      --version   Show version number                                  [boolean]
  -c, --concurrency  max number of concurrent requests to make
                                                          [number] [default: 20]
  -g, --groupIds  security group ids to check              [array] [default: []]
  -o, --output    output format
                            [string] [choices: "tree", "json"] [default: "tree"]
      --help      Show help                                            [boolean]
$ aws-security-group-usage -g sg-abc123

└─┬ sg-abc123: SecurityGroupName
  ├─┬ usage type: instance
  │ └── InstanceId: i-xxx
  └─┬ usage type: dms
    └── ReplicationInstanceIdentifier: my-dms
```

### As a library

```typescript
import { listSecurityGroupUsage } from 'aws-security-group-usage'

listSecurityGroupUsage({ groupIds: [ 'sg-abc123' ] })
  .then(console.log)
```

## Supported usages

- [x] EC2 instances
- [x] VPC endpoints
- [x] Classic ELB
- [x] ELBv2
- [x] RDS
- [x] Lambdas
- [x] ElastiCache
- [x] Route53 Resolvers
- [x] DMS

## API

### groupIds

Type: `string[]`\
Default: `[]`

Which Security Groups to find dependencies for.

### queue

Type: `PQueue`\
Default: `new PQueue({concurrency: 20})`

Queue to manage the number of concurrent requests. See [p-queue](https://github.com/sindresorhus/p-queue).

### ec2

Type: `AWS.EC2`\
Default: `new EC2()`

The AWS EC2 client to use for EC2 API calls.

### elb

Type: `AWS.ELB`\
Default: `new ELB()`

The AWS ELB client to use for ELB API calls.

### elbv2

Type: `AWS.ELBv2`\
Default: `new ELBv2()`

The AWS ELBv2 client to use for ELBv2 API calls.

### rds

Type: `AWS.RDS`\
Default: `new RDS()`

The AWS RDS client to use for RDS API calls.

### lambda

Type: `AWS.Lambda`\
Default: `new Lambda()`

The AWS Lambda client to use for Lambda API calls.

### ec

Type: `AWS.ElastiCache`\
Default: `new ElastiCache()`

The AWS ElastiCache client to use for ElastiCache API calls.

### r53r

Type: `AWS.Route53Resolver`\
Default: `new Route53Resolver()`

The AWS Route53Resolver client to use for Route53Resolver API calls.

### dms

Type: `AWS.DMS`\
Default: `new DMS()`

The AWS DMS client to use for DMS API calls.

## Developing

1. Clone the repo
1. Run `yarn install`
1. Run `yarn test-watch` to run the tests while deving
1. Run `git add . && yarn cm` to commit changes using [commitizen](https://github.com/commitizen/cz-cli)
1. Run `yarn release` to create a new version using [standard-version](https://github.com/conventional-changelog/standard-version)

Lint checks and tests are run automatically on commit and built by the pipeline on push.

## License

aws-security-group-usage is licensed under the terms of the MIT license.
