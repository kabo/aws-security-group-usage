# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.5](https://gitlab.com/kabo/aws-security-group-usage/compare/v0.0.4...v0.0.5) (2021-04-09)


### Features

* support concurrency limiting ([8a3f759](https://gitlab.com/kabo/aws-security-group-usage/commit/8a3f7596a3f90579ff0a7cf97408f56b942bfbb3))

### [0.0.4](https://gitlab.com/kabo/aws-security-group-usage/compare/v0.0.3...v0.0.4) (2021-02-04)

### [0.0.3](https://gitlab.com/kabo/aws-security-group-usage/compare/v0.0.2...v0.0.3) (2021-01-31)

### [0.0.2](https://gitlab.com/kabo/aws-security-group-usagegit/compare/v0.0.1...v0.0.2) (2021-01-31)


### Bug Fixes

* **cli:** cli permissions ([3d5f442](https://gitlab.com/kabo/aws-security-group-usagegit/commit/3d5f4423d4fd16a954d7546b960ac83c29e381b3))

### 0.0.1 (2021-01-31)


### Features

* first release ([d15a31f](https://gitlab.com/kabo/aws-security-group-usage/commit/d15a31f98d41d6d3d3a953a55ad47b35f8825b45))
